#ifndef EVENT_H
#define EVENT_H
struct event
{
	enum type {
		ARRIVAL,
		DEPARTURE,
		OBSERVER
	};
	type eventType;
	double eventTime;
	int packetSize;
	event(type inputType, double time, int size) : eventType(inputType), eventTime(time), packetSize(size){} 
};

#endif
