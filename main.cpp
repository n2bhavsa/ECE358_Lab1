#include <random>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <list>
#include <iterator>

#include "debug.h"
#include "event.h"
#include "DES.h"

using namespace std;

//Decleration of global variables
DiscreteEventSimulator* DiscreteEventSimulator::desInstance = 0;
int nArrivals = 0, nDepartures = 0, nObservers = 0, nIdle = 0, nDrop = 0, nGenerate=0, debug=0;
double previousDepartTime = 0.0, previousObserverTime = 0.0;
double packetsInQueue = 0.0;
//queue and iterator used for MM1K
list<double> departureTimes;
list<double>::iterator nextDepartureEvent;
double totalIdleTime = 0.0;

bool eventCompare2 (event a, event b){
    
    return ((a.eventTime > b.eventTime)?false:true);
}

//event handler for departure event 
void departureEvent( double time){
	nDepartures++;
}

//event handler for arrival event 
void arrivalEvent(list<event>::iterator a, DiscreteEventSimulator* sim){
		nArrivals++;
    	nGenerate++;

	if(sim->getIsInfQueue() == false){
		//if MM1K case, can drop packets
	    if ((nArrivals - nDepartures) > sim->getQueueLength()){
			//buffer full
			nDrop++;
			nArrivals--;
	    }
	    else{
	        //generate departure time based on arrival event 
			double currentDepartureTime = max(a->eventTime,previousDepartTime);
			int currPacketSize = a->packetSize;
			currentDepartureTime += ((double)currPacketSize) / sim->getServiceRate();
			//push departure event into departureTimes queue 
			previousDepartTime = currentDepartureTime;
			departureTimes.push_back(currentDepartureTime);
	    }
	}
	
	//check for any departure time before this event, increment counter and dequeue time 
	//for MM1 case, while loop will exit immediately as queue is empty 
	nextDepartureEvent = departureTimes.begin();
	while(nextDepartureEvent != departureTimes.end() && *nextDepartureEvent < a->eventTime){
		departureEvent(*nextDepartureEvent);
		nextDepartureEvent++;
    	departureTimes.pop_front();
	}	

}

//event handler for observation event 
void observerEvent(list<event>::iterator a){
	//updating counter 
	nObservers++;

	//check for any departure time before this event, increment counter and dequeue time 
	//happens before calculating idle counter 
	//for MM1 case, while loop will exit immediately as queue is empty 
	nextDepartureEvent = departureTimes.begin();
	while(nextDepartureEvent != departureTimes.end() && *nextDepartureEvent < a->eventTime){
		departureEvent(*nextDepartureEvent);
		nextDepartureEvent++;
		departureTimes.pop_front();
	}
	
	//calculate packet in queue with updated counters 
	packetsInQueue+=(nArrivals - nDepartures);
	if(nArrivals - nDepartures == 0){
		nIdle++;
		//calculate total idle time 
		totalIdleTime += (a->eventTime - max(previousDepartTime, previousObserverTime));
//		cout<<"TotalIdleTime: "<<totalIdleTime<<"previousDepartTime: "<<previousDepartTime<<"a->eventTime: "<< a->eventTime<<"\n";
	}
	previousObserverTime = a->eventTime;
}

int main(){
	//initialize variables for simulator  
	int queueLength = 10;
	double arrivalRate = 10, lengthRate = 0.0005, serviceRate =1000000, observationRate = 5*arrivalRate, simTime =1000, totalPackets = 0, lambda = 75;
	//create simulator instance
	DiscreteEventSimulator* simulator = DiscreteEventSimulator::instance(arrivalRate, lengthRate, serviceRate, observationRate, queueLength, simTime, false);
	//create iterator for simulation instance 
	list<event>::iterator it;

	//*****************************************************************************
	//code for Q1 
	//prepare csv file for Q1
	cout<<"Generating thousand random variables, with lambda = 75\n";
	ofstream randomVariables;
	randomVariables.open("randomVariableGenerated.csv");
	double mean = 0.0, variance = 0.0, num;
	for(int i = 0; i < 1000; i++){
		num = simulator->uniformRandomVariable(lambda);
		randomVariables<<num << "\n";
		mean += num;
		variance += pow(num, 2);		
	}
	mean /= 1000;
	variance /= 1000;
	variance -= pow(mean,2);

	cout << "Mean: " << mean <<" Variance: "<< variance << '\n';
	randomVariables<< "Mean: " << mean <<" Variance: "<< variance << '\n';
	randomVariables.close();
	//*****************************************************************************


	//prepare csv for MM1 and MM1K
    ofstream mm1kQueueOutput, mm1QueueOutput;
	mm1kQueueOutput.open("MM1KOutput.csv");
    mm1QueueOutput.open("MM1Output.csv");
    
    mm1QueueOutput <<"nArrivals,  nDepartures,  nObservers, nIdle, Rho,  E(n), P_idle\n";
    mm1kQueueOutput <<"QueueSize, nArrivals,  nDepartures,  nObservers, nIdle, nDrop, nGenerate, Rho,  E(n), P_idle, P_drop\n";
 	cout<<"nArrivals,  nDepartures,  nObservers, nIdle, Rho,  E(n), P_idle\n";


	//*****************************************************************************
	//Code for Q3 and Q4 
	//MM1 Queue with an infinite buffer Simulation
	simulator->setIsInfQueue(true);    
	for(double rho = 0.25; rho < 1.1; rho += 0.1){
		//iterate from 0.25 to 0.95 for Q3 as well as rho = 1.2 for Q4 
		if ((1.05 - rho) < 0.0000001)
		       rho = 1.2;	
		   
		//update arrival rate and observation rate 
		arrivalRate = rho * serviceRate * lengthRate;
		observationRate = 5*arrivalRate;
		//add arrrival, departure and observation events
		simulator->setArrivalRate(arrivalRate);
		simulator->setObserverRate(observationRate);
		simulator->generateArrivalEvents();
		simulator->generateDepartureEvents();
		simulator->generateObserverEvents();
		//add all events to simulator and sort them 
		simulator->generateDES();
	
		//start simulating by dequeuing events 
		for(it = simulator->DES.begin(); it != simulator->DES.end(); it++){
			if(it->eventType == event::ARRIVAL){
				//call arrival event handler 
				arrivalEvent(it, simulator);
			}
			else if(it->eventType == event::DEPARTURE){
				//call departure event handler 
				departureEvent(it->eventTime);	
			}
			else{ 
				//call observation event handler
				observerEvent(it);
			}	
			
		}
	
		
		cout<< nArrivals<< ", "<<nDepartures<<", "<<nObservers<<", "<<nIdle<<", "<< rho <<", "<< packetsInQueue/nObservers <<", " <<((double)(totalIdleTime)/simTime) << "\n";
		//call simulator method to clear all events 
		mm1QueueOutput<< nArrivals<< ", "<<nDepartures<<", "<<nObservers<<", "<<nIdle<<", "<< rho <<", "<< packetsInQueue/nObservers <<", " <<((double)(totalIdleTime)/simTime) << "\n";
		simulator->wipeEvents();
		//reset counter 
		nArrivals = nDepartures = nObservers = nIdle = packetsInQueue = totalIdleTime = previousDepartTime = previousObserverTime = 0;
	}
	//*****************************************************************************
   
	cout<<"QueueSize, nArrivals,  nDepartures,  nObservers, nIdle, nDrop, nGenerate, Rho,  E(n), P_idle, P_drop\n";
	//reset counter for MM1K 
	nArrivals = nDepartures = nObservers = nIdle = nGenerate = nDrop = debug = packetsInQueue = 0;

	//*****************************************************************************
	//code for Q6
	//MM1K queue with a buffer of size K events
	simulator->setIsInfQueue(false);
	//queueLength is initialized to 10 first 
	while (queueLength < 51){
		//update queue length in instance 
		simulator->setQueueLength(queueLength);
		//rho start from 0.5 
		double rho = 0.5;
		while ((1.6 - rho) > 0.000000001){
			arrivalRate = rho * serviceRate * lengthRate;
			//update arrival rate and observation rate 
			simulator->setArrivalRate(arrivalRate);
			simulator->setObserverRate(5*arrivalRate);
			simulator->generateArrivalEvents();
			//departure events has a check, for finite number of buffer size
			//will not be generated 
			simulator->generateDepartureEvents();
			simulator->generateObserverEvents();
			//add all events to queue and sort them 
			simulator->generateDES();
			//initialize previous departure time
			previousDepartTime = 0.0;
		
			//start simulation by dequeuing events 
			for(it = simulator->DES.begin(); it != simulator->DES.end(); it++){
				if(it->eventType == event::ARRIVAL){
					//event handler for arrival events
					arrivalEvent(it, simulator);
				}
				else{ 
					//event handler for Observer Event
					observerEvent(it);
				}	
				
			}
	
			//calculate E[n] 
			double avgPacketsInQueue = 0;
			avgPacketsInQueue = packetsInQueue / nObservers;
			//add any departure time left in depatureTimes queue.
			//if there is departure time happening after all arrival and observation events
			//update counter now 
			//calculate P_loss
   			nDepartures += departureTimes.size();	
			cout << queueLength <<", " << nArrivals << ", " << nDepartures << ", " << nObservers << ", " << nIdle <<", "<<nDrop<<", " << nGenerate<<", "<<rho<<", "<< avgPacketsInQueue <<" , " <<((double)(totalIdleTime)/simTime) <<", "<<(((double)nDrop)/(nGenerate)) << "\n";
			
			//write results to file
			mm1kQueueOutput << queueLength <<", " << nArrivals << ", " << nDepartures << ", " << nObservers << ", " << nIdle <<", "<<nDrop<<", " << nGenerate<<", "<<rho<<", "<< avgPacketsInQueue <<" , " <<((double)(totalIdleTime)/simTime) <<", "<<(((double)nDrop)/(nGenerate)) << "\n";
			//clean events for next simulation 
			simulator->wipeEvents();
			departureTimes.clear();
			//reset counter 
			nArrivals = nDepartures = nObservers = nIdle = nGenerate = nDrop = debug = packetsInQueue = totalIdleTime = previousObserverTime = 0;
			//adjust rho step size 
			if (rho <= 2)
				rho += 0.1;
			//else if (rho <= 5)
			//	rho += 0.2;
			//else if (rho <= 10)
			//	rho += 0.4;
		}

		//change queue length 
		if (queueLength == 10)
			queueLength = 25;
		else if (queueLength == 25)
			queueLength = 50;
		else if (queueLength == 50)
			queueLength = 60;
	}
	//*****************************************************************************
	
	//close file 
	mm1kQueueOutput.close();
	mm1QueueOutput.close();
}


