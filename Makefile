CXX=g++
CXXFLAGS= -std=gnu++11 -I.
DEPS = DES.h debug.h 
OBJ = main.o DES.o
CSV = MM1KOutput.csv MM1Output.csv randomVariableGenerated.csv 

%.o: %.c $(DEPS)
		$(CXX) -c -o $@ $< $(CFLAGS)

default: main.o DES.o 
		$(CXX) -o lab1Output main.o DES.o 
clean: 
	rm -f $(OBJ) lab1Output
cleanall:
	rm -f $(OBJ) $(CSV) lab1Output

