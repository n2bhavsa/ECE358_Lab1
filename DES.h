#ifndef DES_H
#define DES_H

#include <random>
#include <vector>
#include <list>
#include <algorithm>
#include "event.h"

using std::vector;
using std::sort;
using std::list;
using std::default_random_engine;
using std::uniform_real_distribution;
class DiscreteEventSimulator {
	bool isInfQueue;
	int queueLength;
	double arrivalRate, lengthRate, serviceRate, observationRate, totalSimulationTime;
	vector<event> arrivalEvents, departureEvents, observerEvents;
	vector<int> packetLengths;
	static DiscreteEventSimulator *desInstance;

	//using uniform distribution from 0.0 to 1.0
	default_random_engine generator;
	uniform_real_distribution<double> distribution{uniform_real_distribution<double>(0.0, 1.0)};

	//constructor 
	DiscreteEventSimulator(double inArrivalRate, double inLengthRate, double inServiceRate, double inObservationRate, int inQueueLength, double inSimTime, bool inIsInfQueue)
	{
		arrivalRate = inArrivalRate;
		lengthRate = inLengthRate;
		serviceRate = inServiceRate;
		observationRate = inObservationRate;
		queueLength = inQueueLength;
		totalSimulationTime = inSimTime;
		isInfQueue = inIsInfQueue;
	}
	
public:
//	vector<event> DES;
	list<event> DES;
	void setArrivalRate(double inArrivalRate){
		arrivalRate = inArrivalRate;
	}
	void setObserverRate (double inObserverRate){
		observationRate = inObserverRate;
	}	
	static DiscreteEventSimulator* instance(double inArrivalRate, double inLengthRate, double inServiceRate, double inObservationRate, double inQueueLength, double inSimTime, bool inIsInfQueue)
	{
		if(!desInstance){
			desInstance = new DiscreteEventSimulator(inArrivalRate, inLengthRate, inServiceRate, inObservationRate, inQueueLength, inSimTime, inIsInfQueue);
		}
		return desInstance;
	}
	
	double uniformRandomVariable(double rateParam);
	void generateArrivalEvents();
	void generateDepartureEvents();
	void generateObserverEvents();
	void generateDES();
	void wipeEvents();
	void setQueueLength(int);
	int getQueueLength();
	double getServiceRate();
	bool getIsInfQueue();
	void setIsInfQueue(bool);
};	
#endif
