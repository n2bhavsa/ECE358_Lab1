#include "DES.h"
#include <iostream>
using namespace std;

//event compare for sorting function 
bool eventCompare (const event& a, const event& b){

	return ((a.eventTime > b.eventTime)?false:true);
}

double DiscreteEventSimulator::uniformRandomVariable(double rateParam){
	//generate random distribution of number based on formula 
	float uniformRandomVariable = log(1-distribution(generator));
	double returnVariable = -1 * (uniformRandomVariable / rateParam);	
	return returnVariable;
}

void DiscreteEventSimulator::generateArrivalEvents(){
	double currSimTime = 0; //Seconds
	int currPacketLength = 0; //Bits
	//generate all arrival events until meet the simulation time 
	while(totalSimulationTime - currSimTime > 0.0000001){
		//use random number generator to generate arrival interval and packet length 
		currSimTime += uniformRandomVariable(arrivalRate);
		currPacketLength = uniformRandomVariable(lengthRate);
		if(totalSimulationTime - currSimTime > 0.0000001){
			//add arrival event
			packetLengths.push_back(currPacketLength);
			arrivalEvents.push_back(event(event::ARRIVAL, currSimTime, currPacketLength));
		}
	}
}

void DiscreteEventSimulator::generateDepartureEvents(){
	//if finite queue, do not run generate events 
	if(isInfQueue){
		double previousDepartureTime = 0, currentDepartureTime = 0;
		//generate all departure events until meet the simulation time 
		for(int i =0; i < arrivalEvents.size(); i++){
			//for each of the arrival events, calculate the departure time based on 
			//formula Tdep = Max(currentArrival,previousDeparture) + service time 
			//add departure event 
			currentDepartureTime = max(previousDepartureTime, arrivalEvents.at(i).eventTime);
			currentDepartureTime += ((double)arrivalEvents.at(i).packetSize) / serviceRate;
			departureEvents.push_back(event(event::DEPARTURE, currentDepartureTime, arrivalEvents.at(i).packetSize));
			previousDepartureTime = currentDepartureTime;
		}
	}
}

void DiscreteEventSimulator::generateObserverEvents(){
	double currSimTime = 0;
	//generate all observation events until meet the simulation time
	while(totalSimulationTime - currSimTime > 0.000000001){
		//use random number generator to generate observation interval 
		currSimTime += uniformRandomVariable(observationRate);
		if(totalSimulationTime - currSimTime > 0.00000001)
			observerEvents.push_back(event(event::OBSERVER, currSimTime, 0));	
	}
}

void DiscreteEventSimulator::generateDES(){
	//add arrival events to simulator queue
	DES.insert(DES.end(), arrivalEvents.begin(), arrivalEvents.end());
	//add departure events to simulator queue only for MM1 case 
   	if (isInfQueue){
       		 DES.insert(DES.end(), departureEvents.begin(), departureEvents.end());
    	}
	//add observation events to simulator queue 
	DES.insert(DES.end(), observerEvents.begin(), observerEvents.end());
	//sort queue 
	DES.sort(eventCompare);
}

void DiscreteEventSimulator::wipeEvents(){
	//clean all queue and array 
	arrivalEvents.clear();
	departureEvents.clear();
	observerEvents.clear();
	DES.clear();
	packetLengths.clear();	
}

int DiscreteEventSimulator::getQueueLength(){
    return queueLength;
}

double DiscreteEventSimulator::getServiceRate(){
    return serviceRate;
}

void DiscreteEventSimulator::setQueueLength(int length){
	queueLength = length;
}

bool DiscreteEventSimulator::getIsInfQueue(){
	return isInfQueue;
}

void DiscreteEventSimulator::setIsInfQueue(bool inIsInfQueue){
	isInfQueue = inIsInfQueue;
}
