#ifndef DEBUG_H
#define DEBUG_H

#include <vector>
#include <list>
#include <iomanip>
#include <iostream>
#include "DES.h"
#include "event.h"

using std::vector;
using std::list;
using std::cout;
using std::setprecision;

void printEventVector(vector<event> &arrivalEvents, vector<event> &departureEvents){
	for(int i = 0; i < arrivalEvents.size(); i++){
		cout<<"i: " <<i<<setprecision(9)<<" Arrival Time: "<< arrivalEvents.at(i).eventTime << " Departure Time: "<< departureEvents.at(i).eventTime<<" Departure - Arrival "<< departureEvents.at(i).eventTime - arrivalEvents.at(i).eventTime  << "\n";
	}
}

void printDES(DiscreteEventSimulator* sim){
	list<event>::iterator it;
	
	for(it = sim->DES.begin(); it != sim->DES.end(); it++){
		cout<<"Type: "<< it->eventType <<" Time: "<< it->eventTime <<" Packet Size: "<< it->packetSize << "\n"; 
	
	}
}

#endif
